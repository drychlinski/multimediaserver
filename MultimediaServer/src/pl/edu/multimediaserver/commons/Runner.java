package pl.edu.multimediaserver.commons;

import com.sun.jna.NativeLibrary;

import pl.edu.multimediaserver.controllers.Controller;
import pl.edu.multimediaserver.models.AppModel;
import pl.edu.multimediaserver.views.ApplicationView;

public class Runner {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		NativeLibrary.addSearchPath("libvlc", "//home//damian//Projekty//git//MultimediaServer//libs");
		new Controller();
		//AppModel appModel = new AppModel(null);
		//appModel.start();

	}
}