package pl.edu.multimediaserver.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

import pl.edu.multimediaserver.controllers.Controller;

public class AppModel {
	
	private Controller controller;
	private Properties properties;
	
	public AppModel(Controller controller){
		this.controller = controller;
	}

	public void zapisz(){
		String adres, port, protokol;
		adres = controller.getFrame().getTfAdres().getText().toString();
		port = controller.getFrame().getTfPort().getText().toString();
		protokol = controller.getFrame().getsProtokol().getValue().toString();
		
		properties = new Properties();
		
		properties.put("adres", adres);
		properties.put("port", port);
		properties.put("protokol", protokol);
		
		try {
			FileOutputStream fos = new FileOutputStream("ustawienia.xml");
			properties.storeToXML(fos, "UstawieniaAplikacji");
		} catch (FileNotFoundException e1) {
			JOptionPane.showMessageDialog(controller.getFrame(),"Błąd " + e1);
			e1.printStackTrace();
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(controller.getFrame(),"Błąd " + e1);
			e1.printStackTrace();
		}
	}
	public void wczytaj(){
		properties = new Properties();
		File input = null;
	 
		try {
			input = new File("ustawienia.xml");
			
			FileInputStream fs = new FileInputStream(input);
			
			properties.loadFromXML(fs);
			
			int indProtokol =-1;
	 
			// get the property value and print it out
			String adres = properties.getProperty("adres");
			String port = properties.getProperty("port");
			String protokol = properties.getProperty("protokol");
			
			switch(protokol){
			case "HTTP":
				indProtokol = 0;
				break;
			case "RSTP":
				indProtokol = 1;
				break;
			default:
	             throw new IllegalArgumentException("Niepoprawnie wczytany protokół");
			}
			
			controller.getFrame().setTfAdres(adres);
			controller.getFrame().setTfPort(port);
			controller.getFrame().setsProtokol(indProtokol);
	 
		} catch (IOException | IllegalArgumentException | NullPointerException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				//try {
					//close
				//} catch (IOException e) {
				//	e.printStackTrace();
				//}
			}
		}
	}
	
	public void start(){
		File f = new File("ustawienia.xml");
		if(f.exists() && !f.isDirectory()) { 
			wczytaj();
		}
	}
}
