package pl.edu.multimediaserver.views;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;

import java.awt.Canvas;

public class ApplicationView extends JFrame {

	//region pola klasy
	
	//Panel
	private JPanel contentPane;
	public JPanel jPanelCenter;
	private JPanel jPanelEast;
	private JPanel jPanelSouth;
	
	//Menu
	private JMenuBar menuBar;
	private JMenu mnStream;
	private JMenuItem mntmFile;
	private JMenuItem mntmDevice;
	private JMenuItem mntmExit;
	private JTextField tfAdres;
	private JButton btnConnect;
	private JButton btnSave;
	private JLabel lblAdres;
	private JLabel lblPort;
	private JTextField tfPort;
	private JLabel lblRodzaj;
	private JSpinner sProtokol;
	public Canvas canvas;
	//endregion
	
	//region Gettery i Settery
	//Gettery
	public JMenuItem getMntmFile() {
		return mntmFile;
	}

	public void setTfAdres(String adres) {
		this.tfAdres.setText(adres);
	}

	public void setTfPort(String port) {
		this.tfPort.setText(port);
	}

	public void setsProtokol(int nr) {
		this.sProtokol.getModel().setValue(nr);
	}

	public JMenuItem getMntmDevice() {
		return mntmDevice;
	}

	public JMenuItem getMntmExit() {
		return mntmExit;
	}
	
	public JButton getBtnConnect() {
		return btnConnect;
	}

	public JButton getBtnSave() {
		return btnSave;
	}
	
	public JTextField getTfAdres() {
		return tfAdres;
	}

	public JTextField getTfPort() {
		return tfPort;
	}

	public JSpinner getsProtokol() {
		return sProtokol;
	}
	//endregion
	
	public JPanel getjPanelCenter() {
		return jPanelCenter;
	}

	public void setjPanelCenter(JPanel jPanelCenter) {
		this.jPanelCenter = jPanelCenter;
	}

	//region Listenery
	public void addMenuListener(ActionListener al){
		mntmFile.addActionListener(al);
		mntmDevice.addActionListener(al);
		mntmExit.addActionListener(al);
	}
	
	public void addEastListener(ActionListener al){
		btnConnect.addActionListener(al);
		btnSave.addActionListener(al);
	}
	//endregion
	
	/**
	 * Create the frame.
	 */
	public ApplicationView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		
		//region Menu
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnStream = new JMenu("Stream");
		menuBar.add(mnStream);
		
		mntmFile = new JMenuItem("File");
		mnStream.add(mntmFile);
		
		mntmDevice = new JMenuItem("Device");
		mnStream.add(mntmDevice);
		
		mntmExit = new JMenuItem("Exit");
		mnStream.add(mntmExit);
		//endregion
		
		//region Panel
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		setjPanelCenter(new JPanel());
		contentPane.add(getjPanelCenter(), BorderLayout.CENTER);
		
		//region CENTER VIEW
		canvas = new Canvas();
		
	    this.getjPanelCenter().setBackground(Color.black);
		this.getjPanelCenter().setPreferredSize(new Dimension(640,480));
		this.getjPanelCenter().setLayout(new BorderLayout());
		this.getjPanelCenter().add(canvas, BorderLayout.CENTER);
		//endregion
		
		jPanelEast = new JPanel();
		contentPane.add(jPanelEast, BorderLayout.EAST);
		
		jPanelSouth = new JPanel();
		contentPane.add(jPanelSouth, BorderLayout.SOUTH);
		//endregion
		
		//region EAST
		
		GridBagLayout gbl_jPanelEast = new GridBagLayout();
		gbl_jPanelEast.columnWidths = new int[] {114, 117};
		gbl_jPanelEast.rowHeights = new int[]{25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_jPanelEast.columnWeights = new double[]{1.0, 0.0};
		gbl_jPanelEast.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		jPanelEast.setLayout(gbl_jPanelEast);
		
		lblAdres = new JLabel("Adres:");
		GridBagConstraints gbc_lblAdres = new GridBagConstraints();
		gbc_lblAdres.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdres.gridx = 0;
		gbc_lblAdres.gridy = 0;
		jPanelEast.add(lblAdres, gbc_lblAdres);
		
		tfAdres = new JTextField();
		GridBagConstraints gbc_tfAdres = new GridBagConstraints();
		gbc_tfAdres.gridwidth = 2;
		gbc_tfAdres.insets = new Insets(0, 0, 5, 0);
		gbc_tfAdres.gridx = 0;
		gbc_tfAdres.gridy = 1;
		jPanelEast.add(tfAdres, gbc_tfAdres);
		tfAdres.setColumns(20);
		
		lblPort = new JLabel("Port:");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.anchor = GridBagConstraints.EAST;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 0;
		gbc_lblPort.gridy = 3;
		jPanelEast.add(lblPort, gbc_lblPort);
		
		tfPort = new JTextField();
		GridBagConstraints gbc_tfPort = new GridBagConstraints();
		gbc_tfPort.anchor = GridBagConstraints.WEST;
		gbc_tfPort.insets = new Insets(0, 0, 5, 0);
		gbc_tfPort.gridx = 1;
		gbc_tfPort.gridy = 3;
		jPanelEast.add(tfPort, gbc_tfPort);
		tfPort.setColumns(5);
		
		lblRodzaj = new JLabel("Rodzaj:");
		GridBagConstraints gbc_lblRodzaj = new GridBagConstraints();
		gbc_lblRodzaj.insets = new Insets(0, 0, 5, 5);
		gbc_lblRodzaj.gridx = 0;
		gbc_lblRodzaj.gridy = 5;
		jPanelEast.add(lblRodzaj, gbc_lblRodzaj);
		
		sProtokol = new JSpinner();
		sProtokol.setModel(new SpinnerListModel(new String[] {"HTTP", "RSTP"}));
		GridBagConstraints gbc_sProtokol = new GridBagConstraints();
		gbc_sProtokol.anchor = GridBagConstraints.WEST;
		gbc_sProtokol.insets = new Insets(0, 0, 5, 0);
		gbc_sProtokol.gridx = 1;
		gbc_sProtokol.gridy = 5;
		jPanelEast.add(sProtokol, gbc_sProtokol);
		
		btnSave = new JButton("Save");
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.anchor = GridBagConstraints.NORTH;
		gbc_btnSave.insets = new Insets(0, 0, 0, 5);
		gbc_btnSave.gridx = 0;
		gbc_btnSave.gridy = 13;
		jPanelEast.add(btnSave, gbc_btnSave);
		
		btnConnect = new JButton("Connect");
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.anchor = GridBagConstraints.NORTH;
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 13;
		jPanelEast.add(btnConnect, gbc_btnConnect);
		//endregion
		
		//region SOUTH
		
		JLabel lblSouth = new JLabel("South");
		jPanelSouth.add(lblSouth);
		//endregion
		
		
	}
}
