package pl.edu.multimediaserver.controllers;

import java.awt.EventQueue;

import pl.edu.multimediaserver.models.AppModel;
import pl.edu.multimediaserver.views.ApplicationView;

public class Controller {

	private ApplicationView frame;
	public ApplicationView getFrame() {
		return frame;
	}

	private MenuListener menuListen = new MenuListener(this);
	private EastListener eastListen = new EastListener(this);
	private AppModel appModel = new AppModel(this);
	
	
	public Controller(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new ApplicationView();
					frame.setVisible(true);
					//menuListen = new MenuListener(this);
					frame.addMenuListener(menuListen);
					frame.addEastListener(eastListen);
					appModel.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}
}
