package pl.edu.multimediaserver.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

import pl.edu.multimediaserver.models.AppModel;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;

public class EastListener implements ActionListener{
	private Controller controller;
	private Properties properties;

	public EastListener(Controller controller){
		this.controller = controller;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		
		//region Save
		if(src==controller.getFrame().getBtnSave()){
			AppModel zap = new AppModel(controller);
			zap.zapisz();
		}
		//endregion
		
		//region Connect
		if(src == controller.getFrame().getBtnConnect()){
			//JOptionPane.showMessageDialog(controller.getFrame(),"Działa");
			
			String adres, port, protokol;
			adres = controller.getFrame().getTfAdres().getText();
			port = controller.getFrame().getTfPort().getText();
			protokol = controller.getFrame().getsProtokol().getValue().toString();
			
			MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
			CanvasVideoSurface videoSurface = mediaPlayerFactory.newVideoSurface(controller.getFrame().canvas);
			EmbeddedMediaPlayer mediaPlayer = mediaPlayerFactory.newEmbeddedMediaPlayer();
			mediaPlayer.setVideoSurface(videoSurface);
			controller.getFrame().jPanelCenter.add(controller.getFrame().canvas);
			
			//his.validate();
			String url = protokol + "://" + adres + ":" + port; 
			mediaPlayer.playMedia(url);
		}
		//endregion
	}
}
