package pl.edu.multimediaserver.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.player.embedded.videosurface.CanvasVideoSurface;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

public class MenuListener implements ActionListener{
	private Controller controller;

	public MenuListener(Controller controller){
		this.controller = controller;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		
		//region File
		if(src==controller.getFrame().getMntmFile()){
			MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
			CanvasVideoSurface videoSurface = mediaPlayerFactory.newVideoSurface(controller.getFrame().canvas);
			HeadlessMediaPlayer mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
			controller.getFrame().jPanelCenter.add(controller.getFrame().canvas);
			
			String url = null;
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			int result = fileChooser.showOpenDialog(controller.getFrame());
			if (result == JFileChooser.APPROVE_OPTION) {
			    File selectedFile = fileChooser.getSelectedFile();
			    url = selectedFile.getAbsolutePath();
			}
			mediaPlayer.playMedia(url,":sout=#duplicate{dst=std{access=http,mux=ts,dst=:8080}}");
		}
		//endregion
		
		//region Device
		if(src == controller.getFrame().getMntmDevice()){
			MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
			CanvasVideoSurface videoSurface = mediaPlayerFactory.newVideoSurface(controller.getFrame().canvas);
			HeadlessMediaPlayer mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
			controller.getFrame().jPanelCenter.add(controller.getFrame().canvas);
			
			//http + kodowanie
			mediaPlayer.playMedia("v4l2:///dev/video0",new String[] { ":v4l2-standard=", 
					":live-caching=300", 
					":sout=#transcode{vcodec=VP80,vb=800,scale=1}:http{mux=webm, dst=:8080}"});
			
			//rtsp + kodowanie
			//mediaPlayer.playMedia("v4l2:///dev/video0",new String[] { ":v4l2-standard=", 
			//		":live-caching=300", 
			//		":sout=#transcode{vcodec=mp1v,vb=800,scale=1}:rtp{mux=ts, sdp=rtsp://:8554/stream.sdp}"});			
		}
		//endregion
		
		//region Exit
		if(src == controller.getFrame().getMntmExit()){
			System.exit(0);
		}
		//endregion
	}
}
